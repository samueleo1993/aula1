// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyC8BIjf8Wbpor7ns9dZPuMAyAMDf4G2H9Y",
    authDomain: "controle-samuel.firebaseapp.com",
    projectId: "controle-samuel",
    storageBucket: "controle-samuel.appspot.com",
    messagingSenderId: "197974433639",
    appId: "1:197974433639:web:d3abe303b84857226cd9c2",
    measurementId: "G-HRLEGN9WT8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
