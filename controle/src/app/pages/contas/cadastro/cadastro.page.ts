import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ContaService } from '../service/conta-service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  cadastroForm: FormGroup;

  constructor(
    private builder: FormBuilder,
    private conta: ContaService,
    private nav: NavController
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm(){
    this.cadastroForm = this.builder.group({
      tipo: ['', [Validators.required]],
      valor: ['', [Validators.required, Validators.min(0.01)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]],
      parceiro: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  registraConta(){
    const conta = this.cadastroForm.value;
    this.conta.registraConta(conta).then(() => this.nav.navigateForward('contas/pagar'));
  }
}
